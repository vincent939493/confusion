# Front-End Web Development with React

Topics:

- React Components
- React Router
- React Redux Form
- Redux Actions
- json-server
- fetch
- React Animations